<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/songs', 'SongController@index');
Route::get('/songs/new', 'SongController@create'); // show create song form
Route::post('/songs', 'SongController@store'); // handle create song form submit
Route::get('/songs/{id}/delete', 'SongController@destroy');
